﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace IEEEAttendance
{
    class Member
    {
        public string firstName;
        public string lastName;
        public string email;
        public string studentID;
        public string phone;

        /// <summary>
        /// This method goes through the database and finds a member by studentID
        /// </summary>
        /// <param name="studentID"></param>
        /// <returns></returns>
        public static Member FindMember(string studentID)
        {
            IEEEDataSetTableAdapters.MembersTableAdapter adap = new IEEEDataSetTableAdapters.MembersTableAdapter();
            IEEEAttendance.IEEEDataSet.MembersDataTable dt = adap.GetData();

            foreach (DataRow dr in dt.Rows)
            {
                string currStudID = dr["Student ID"].ToString();
                if (currStudID == studentID) { return convertDataRowToMember(dr); }
            }

            return null; //if we reach this there was no member with this studentID
        }

        /// <summary>
        /// This method takes a given datarow, and populates a member object
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private static Member convertDataRowToMember(DataRow dr)
        {
            Member mem = new Member();
            mem.studentID = dr["Student ID"].ToString();
            mem.firstName = dr["First Name"].ToString();
            mem.lastName = dr["Last Name"].ToString();
            mem.email = dr["Email"].ToString();
            mem.phone = dr["Phone Number"].ToString();

            return mem;
        }

        /// <summary>
        /// adds a member to the db table
        /// </summary>
        /// <returns></returns>
        public bool addMember()
        {
            IEEEDataSetTableAdapters.MembersTableAdapter adap = new IEEEDataSetTableAdapters.MembersTableAdapter();
            int result = adap.Insert(studentID, lastName, firstName, email, phone);

            if (result == 1) return true;
            else return false;
        }



    }



}
