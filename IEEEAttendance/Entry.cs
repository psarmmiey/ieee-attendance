﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace IEEEAttendance
{
    class Entry
    {
        public string studentID;
        public string dateCreated;
        public string lastName;
        public string firstName;

        /// <summary>
        /// A constructor for entry
        /// </summary>
        /// <param name="StudentID"></param>
        /// <param name="LastName"></param>
        /// <param name="FirstName"></param>
        public Entry(string StudentID, string LastName, string FirstName)
        {
            studentID = StudentID;
            lastName = LastName;
            firstName = FirstName;
            dateCreated = System.DateTime.Now.ToString();
        }

        /// <summary>
        /// Adds an entry to the db
        /// </summary>
        /// <returns></returns>
        public bool addEntry()
        {
            IEEEDataSetTableAdapters.EntriesTableAdapter adap = new IEEEDataSetTableAdapters.EntriesTableAdapter();
            int result = adap.Insert(studentID, dateCreated, lastName, firstName);

            if (result == 1) return true;
            else return false;

        }

        /// <summary>
        /// Overloads the ToString Operation to print something useful
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return dateCreated + " : " + lastName + ", " + firstName
                + " (" + studentID + ")";
        }
    }



}
