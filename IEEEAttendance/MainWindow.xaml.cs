﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;


namespace IEEEAttendance
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Timer timer = new Timer();
        string defaultPromptText = "Please input your 10 digit student identification number...";
        int notificationTime = 3;//3 seconds
        int notificationTimeError = 4;//4 seconds

        public MainWindow()
        {
            InitializeComponent();
            this.KeyDown += new KeyEventHandler(keyed);
            prompt.Text = defaultPromptText;
        }


        /// <summary>
        /// Event is called on keypresses. Handle the KeyDown event to determine if a student ID is being input...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void keyed(object sender, KeyEventArgs e)
        {
            //if the user enters a digit into the keyboard
            if (e.Key == Key.D0 || e.Key == Key.D1 || e.Key == Key.D2 || e.Key == Key.D3 || e.Key == Key.D4 ||
                e.Key == Key.D5 || e.Key == Key.D6 || e.Key == Key.D7 || e.Key == Key.D8 || e.Key == Key.D9 ||
                e.Key == Key.NumPad0 || e.Key == Key.NumPad1 || e.Key == Key.NumPad2 || e.Key == Key.NumPad3 ||
                e.Key == Key.NumPad4 || e.Key == Key.NumPad5 || e.Key == Key.NumPad6 || e.Key == Key.NumPad7 ||
                e.Key == Key.NumPad8 || e.Key == Key.NumPad9)
            {
                studentIDInput.Text += e.Key.ToString().Substring(e.Key.ToString().Length - 1);
                if (studentIDInput.Text.Length == 10)
                {
                    processUserLogin(studentIDInput.Text);

                    //Clear the Text Field
                    studentIDInput.Text = "";
                }
            }

        //otherwise, we should blank the text input
            else
            {
                studentIDInput.Text = "";
            }
        }


        /// <summary>
        /// This is called by the keyed event.  It will process a user login
        /// </summary>
        /// <param name="studentID"></param>
        private void processUserLogin(String studentID)
        {
            Member mem = null;
            //Does this User Exist in the System
            mem = Member.FindMember(studentID);

            //If User Does Not Exist, alert the user to reenter or to create member
            if (mem == null)
            {
                changePromptText("The Entered ID was not found in the database.  Reenter your ID or add yourself as a member.", notificationTimeError);
            }
            else
            {
                //Add Entry to DB
                Entry entry = new Entry(mem.studentID, mem.lastName, mem.firstName);
                bool succ = entry.addEntry();
                if (!succ) changePromptText("Failed to add Member Date Entry to the db", notificationTime);
                else
                {
                    changePromptText("Hello " + entry.firstName + " " + entry.lastName, notificationTime);
                    //Print Login to Screen
                    attendees.Text = entry.ToString() + " \n" + attendees.Text;
                }
            }
        }


        /// <summary>
        /// opens the Member Creation Window for the user to create a new member
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addMember_Click(object sender, RoutedEventArgs e)
        {
            var newWindow = new MemberCreation();
            newWindow.Show();
        }

        /// <summary>
        /// This Method changes the prompt text to a selected message for a set time, then returns to the default message
        /// </summary>
        /// <param name="text"></param>
        /// <param name="secondsToLeaveChangedFromDefault"></param>
        private void changePromptText(string text, int secondsToLeaveChangedFromDefault)
        {
            prompt.Text = text;

            timer.Interval = (double)secondsToLeaveChangedFromDefault * 1000;
            timer.Elapsed += new ElapsedEventHandler(OnTimerElapsed);
            timer.Start();
        }
        /// <summary>
        /// This is a helper method for the changePromptTextMessage
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimerElapsed(object source, ElapsedEventArgs e) 
        { 
            prompt.Dispatcher.Invoke(
                System.Windows.Threading.DispatcherPriority.Normal, 
                new Action(
                    delegate(){
                        prompt.Text = defaultPromptText;
                    }
                )
            );
            timer.Stop();
        }

    }
}
