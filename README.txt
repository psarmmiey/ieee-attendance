Started On: December 19, 2010

This application was created to simplify the process of taking attendance. 
Previously, attendees would write their information on a sheet of paper. 
Later another individual would need to take the time to enter the data into a
database. Now, attendees can simply type in their 10 digit ID and they are 
ready to go. More conveniently, a magnetic card reader can be used allowing 
the user to swipe their student ID card. 
